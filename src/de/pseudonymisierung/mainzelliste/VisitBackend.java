package de.pseudonymisierung.mainzelliste;

import de.pseudonymisierung.mainzelliste.Config;
import de.pseudonymisierung.mainzelliste.Visit;
import de.pseudonymisierung.mainzelliste.dto.Persistor;

public class VisitBackend {
  /**
   * Retrieves a visit from the Persistor by idString + visitName.
   */
  public static Visit getVisit(String visitIdString, String visitName) {
    String configuredGiven = Config.instance.getProperty("visit." + visitName + ".given");
    String configuredGenerated = Config.instance.getProperty("visit." + visitName + ".generated");

    ID givenID = Persistor.instance.getId(visitIdString, configuredGiven);
    ID generatedID = Persistor.instance.getId(visitIdString, configuredGenerated);

    if(givenID != null) {
      return Persistor.instance.getVisit(givenID);
    }
    if(generatedID != null) {
      return Persistor.instance.getVisit(generatedID);
    }

    return null;
  }

  /**
   * Creates a visit using if not already available.
   */
  public static Visit createVisit(String visitIdString, String visitName) {
    String configuredGiven = Config.instance.getProperty("visit." + visitName + ".given");
    String configuredGenerated = Config.instance.getProperty("visit." + visitName + ".generated");

    Visit visit = VisitBackend.getVisit(visitIdString, visitName);
    if(visit != null) {
      return visit;
    } 

    ID givenID = IDGeneratorFactory.instance.buildId(configuredGiven, visitIdString);
    ID generatedID = IDGeneratorFactory.instance.getFactory(configuredGenerated).getNext();
    visit = new Visit(givenID, generatedID);
    Persistor.instance.updateVisit(visit);

    return visit;
  }
}
