package de.pseudonymisierung.mainzelliste.webservice;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.json.JSONException;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.dstu3.model.Parameters;
import org.hl7.fhir.dstu3.model.Parameters.ParametersParameterComponent;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Identifier.IdentifierUse;
import org.hl7.fhir.dstu3.model.OperationOutcome;
import org.hl7.fhir.dstu3.model.OperationOutcome.OperationOutcomeIssueComponent;

import de.pseudonymisierung.mainzelliste.Servers;
import de.pseudonymisierung.mainzelliste.Visit;
import de.pseudonymisierung.mainzelliste.VisitBackend;

/**
 * Resources to communicate using the standard defined in IHE ITI-TF PIXm 
 */

@Path("/Visit")
public class VisitResource {

  private Logger logger = Logger.getLogger(VisitResource.class);
  
  @GET
  @Produces({MediaType.APPLICATION_JSON})
  public Response visitResponse(@Context HttpServletRequest req,
      @QueryParam("visitString") String visitString,
      @QueryParam("visitType") String visitName) {
    
    Servers.instance.checkPermission(req, "createSession");
    Servers.instance.checkPermission(req, "createToken");
    Servers.instance.checkPermission(req, "tt_addPatient");
    
    logger.info("/Visit GET request received.");

    Visit visit = VisitBackend.getVisit(visitString, visitName);
    if(visit == null) {
      return Response.status(404).build();
    } else {
      return Response.status(200).entity(visit.toJSON()).build();
    }
  }

  @PUT
  @Produces({MediaType.APPLICATION_JSON})
  public Response createVisit(@Context HttpServletRequest req,
      @QueryParam("visitString") String visitGiven,
      @QueryParam("visitType") String visitName) {

    Servers.instance.checkPermission(req, "createSession");
    Servers.instance.checkPermission(req, "createToken");
    Servers.instance.checkPermission(req, "tt_addPatient");

    logger.info("/Visit PUT request received.");

    return this.createVisit(visitGiven, visitName);
  }

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces({MediaType.APPLICATION_JSON})
  public Response createVisitPost(@Context HttpServletRequest req, 
      JSONObject body) {
    Servers.instance.checkPermission(req, "createSession");
    Servers.instance.checkPermission(req, "createToken");
    Servers.instance.checkPermission(req, "tt_addPatient");

    logger.info("/Visit POST request received.");

    String visitString;
    String visitName;
    try {
      visitString = body.getString("visitString");
      visitName = body.getString("visitName");
    } catch(JSONException e) {
      return null;
    }
    return this.createVisit(visitString, visitName);
  }

  Response createVisit(String visitGiven, String visitName) {
    Visit visit = VisitBackend.getVisit(visitGiven, visitName);
    if(visit != null) {
        return Response.status(201).entity(visit.toJSON()).build();
    }
    visit = VisitBackend.createVisit(visitGiven, visitName);
    if(visit != null) {
      return Response.status(201).entity(visit.toJSON()).build();
    }
    return Response.serverError().build();
  }
}
