package de.pseudonymisierung.mainzelliste.webservice;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;

import org.apache.log4j.Logger;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.dstu3.model.Parameters;
import org.hl7.fhir.dstu3.model.Parameters.ParametersParameterComponent;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Identifier.IdentifierUse;
import org.hl7.fhir.dstu3.model.OperationOutcome;
import org.hl7.fhir.dstu3.model.OperationOutcome.OperationOutcomeIssueComponent;

import de.pseudonymisierung.mainzelliste.Config;
import de.pseudonymisierung.mainzelliste.ID;
import de.pseudonymisierung.mainzelliste.IDGeneratorFactory;
import de.pseudonymisierung.mainzelliste.Patient;
import de.pseudonymisierung.mainzelliste.Servers;
import de.pseudonymisierung.mainzelliste.dto.Persistor;

import com.sun.jersey.spi.resource.Singleton;

/**
 * Resources to communicate using the standard defined in IHE ITI-TF PIXm 
 */

@Path("/Patient")
@Singleton
public class IHEPIXmResource {

  /**
   * Logging
   */
  private Logger logger = Logger.getLogger(IHEPIXmResource.class);
  private FhirContext dstu3 = FhirContext.forDstu3();

  @GET
  @Path("/$ihe-pix")
  @Produces({"application/fhir+xml,application/fhir+json,application/xml,application/json"})
  public Response pixResponse(@Context HttpServletRequest req,
      @QueryParam("sourceIdentifier") String sourceIdentifier,
      @QueryParam("targetSystem") String targetSystem,
      @QueryParam("_format") String format,
      @QueryParam("_pretty") String pretty) {
      
      if(!Boolean.parseBoolean(Config.instance.getProperty("ihe.enabled"))) {
        return Response.status(404).build();
      }
      // A apiKey should be allowed to createSessions, createTokens, addPatients as well as editPatients
      Servers.instance.checkPermission(req, "iheAccess");
      Servers.instance.checkPermission(req, "pixmQuery");

      // TODO: audit relevant info here
      logger.info("Received GET /Patient");

      String[] srcIdentifier = this.parseSourceIdentifier(sourceIdentifier);
      if(srcIdentifier == null) {
        // if srcIdentifier (the only mandatory argument) is not parsable, we have a bad request
        return Response.status(400).build();
      }
      String sourceSystem = srcIdentifier[0];
      String sourcePatientIdentifier = srcIdentifier[1];

      // from here on, all responses shall be FHIR Resources
      
      // Define the respond format here
      IParser dstu3Parser = this.setParser(req.getHeader("Accept"), format, pretty);
      List<String> mainzellisteIdTypes = Arrays.asList(IDGeneratorFactory.instance.getIDTypes());
      int responseStatus = 0;
      Resource responseResource = null;

      //
      // Handle all cases from the IHE PIXm Supplement
      //
      
      // Case 4: sourceSystem not recognized
      if(!mainzellisteIdTypes.contains(sourceSystem)) {
        responseResource = this.case4_assigningAuthorityNotFound();
        responseStatus = 400;
      }

      // Case 5: targetSystem not recognized
      else if(targetSystem != null && !mainzellisteIdTypes.contains(targetSystem)) {
          responseResource = this.case5_targetSystemNotFound();
          responseStatus = 403;

      // valid sourceSystem and valid targetSystem (if set)
      } else {
        ID patientId = IDGeneratorFactory.instance.buildId(sourceSystem, sourcePatientIdentifier);
        Patient p = Persistor.instance.getPatient(patientId);

        // Case 3: patient with sourceIdentifier not found 
        if(p == null) {
          responseResource = this.case3_patientIdentifierNotFound();
          responseStatus = 404;

        // patient found, all cases return a parameters response with a 200 status
        // Case 2: if no identifiers are available, the returned Resource will be empty
        } else {
          responseStatus = 200;

          // Case 1: return all known identifiers of a patient
          if(targetSystem == null) {
            responseResource = this.case1_returnAllIdentifiers(p, sourceSystem);

          // Case 6: return identifiers specified by targetSystem
          } else {
            responseResource = this.case6_returnTargetIdentifier(p, sourceSystem, targetSystem);
          }
        }
      }

      return Response.status(responseStatus)
        .entity(dstu3Parser.encodeResourceToString(responseResource))
        .build();
  }

  /**
   * Creates a response FHIR Resource for IHE PIXm Case 1, retrieving all available patientIdentifiers.
   */
  private Resource case1_returnAllIdentifiers(Patient p, String sourceIdType) {
    Parameters parameters = new Parameters();
    for(ID patId: p.getIds()) {
      if(!patId.getType().equals(sourceIdType)) {
        parameters.addParameter(mainzellisteIDToFHIRIdentifier(patId));
      }
    }
    logger.info("Case 1 successfully handled.");
    return parameters;
  }

  /**
   * Creates a response FHIR Resource for IHE PIXm Case 4, assigning authority of the sourceIdentifier not found.
   */
  private Resource case3_patientIdentifierNotFound() {
      OperationOutcome oo = createOOBase();
      oo.getIssue().get(0).setCode(OperationOutcome.IssueType.NOTFOUND);
      oo.getIssue().get(0).setDiagnostics("sourceIdentifier Patient Identifier not found");
      logger.info("Case 3 successfully handled.");
      return oo;
  }

  /**
   * Creates a response FHIR Resource for IHE PIXm Case 4, assigning authority of the sourceIdentifier not found.
   */
  private Resource case4_assigningAuthorityNotFound() {
      OperationOutcome oo = createOOBase();
      oo.getIssue().get(0).setDiagnostics("sourceIdentifier Assigning Authority not found");
      logger.info("Case 4 successfully handled.");
      return oo;
  }

  /**
   * Creates a response FHIR Resource for IHE PIXm Case 5, targetSystem set but not found.
   */
  private Resource case5_targetSystemNotFound() {
      OperationOutcome oo = createOOBase();
      oo.getIssue().get(0).setDiagnostics("targetSystem not found");
      logger.info("Case 5 successfully handled.");
      return oo;
  }

  /**
   * Creates a response FHIR Resource for IHE PIXm Case 6, retrieving a patientIdenifier based on a targetSystem.
   */
  private Resource case6_returnTargetIdentifier(Patient p, String sourceIdType, String targetIdType) {
    Parameters parameters = new Parameters();
    for(ID patId: p.getIds()) {
      if(patId.getType().equals(targetIdType)) {
        parameters.addParameter(this.mainzellisteIDToFHIRIdentifier(p.getId(targetIdType)));
      }
    }
    logger.info("Case 6 successfully handled.");
    return parameters;
  }

  /**
   * Create a OperationOutcome that only needs diagnostics for the first Issue to be set.
   */
  private OperationOutcome createOOBase() {
      OperationOutcome oo = new OperationOutcome();
      OperationOutcomeIssueComponent ooic = new OperationOutcomeIssueComponent();
      ooic.setSeverity(OperationOutcome.IssueSeverity.ERROR);
      ooic.setCode(OperationOutcome.IssueType.CODEINVALID);
      oo.addIssue(ooic);
      return oo;
  }

  /**
   * Trasforms Mainzelliste ID Objects to the ParameterComponents needed to build the response FHIR resources.
   */
  private ParametersParameterComponent mainzellisteIDToFHIRIdentifier(ID id) {
    // TODO: there should be some possibility to set access rights to certain PIDs (per ApiKey maybe?)
    Identifier identifier = new Identifier();
    identifier.setSystem(id.getType())
      .setValue(id.getIdString())
      .setUse(IdentifierUse.OFFICIAL);
    ParametersParameterComponent paramComponent = new ParametersParameterComponent();
    paramComponent.setName("targetIdentifier")
      .setValue(identifier);
    return paramComponent;
  }

  /**
   * Checks if a mediaTypeString is equal to either one of the possibilities of FHIR MIME-Types.
   */
  private boolean acceptIs(String mediaTypeString, String type) {
    return mediaTypeString.equals(type) 
        || mediaTypeString.equals("application/" + type) 
        || mediaTypeString.equals("application/fhir+" + type) 
        || mediaTypeString.equals("application/" + type + "+fhir");

  }

  /**
   * Parse a source identifier supplied by the GET request according to IHE PIXm.
   *
   * @param sourceIdentifier    A string that includes the Assigning Authority and the identifier value separated by a "|", e.g.: urn:oid:1.3.6.1.4.1.21367.2010.1.2.300|NA5404
   * @return An String array that contains the Assigning Authority as the 0th element and the identifier value as the 1st element if the sourceIdentifier is valid, null otherwise.
   */
  private String[] parseSourceIdentifier(String sourceIdentifier) {
    if(!sourceIdentifier.contains("|")) {
        return null;
    }
    String[] srcIdentifiers = sourceIdentifier.split("\\|");
    if(srcIdentifiers.length != 2) {
        return null;
    }
    return srcIdentifiers;
  }

  /**
   * Sets the parser according to the rules supplied by HTTP header and the _foramt and _pretty query parameters.
   */
  private IParser setParser(String headerAcceptValue, String format, String pretty) {
      IParser dstu3Parser = dstu3.newJsonParser();
      /*
       * boolean supportedAcceptType = false; 
       * Initially, this should have been an indicator whether a valid MIME-Type
       * has been supplied. This should be handled by Servlet / Tomcat / JAXRS already.
       */
      if(format != null) {
        // format overrides the Accept-header
        headerAcceptValue = format;
      }
      if(headerAcceptValue != null) {
        // without format, the last accept value is prioritized
        // TODO: RFC 7231 states , for multiple Accept MIME-Types. why ';'?
        for(String accept: headerAcceptValue.split(";")) {
          if(acceptIs(accept.trim(), "json")) {
            dstu3Parser = dstu3.newJsonParser();
            //supportedAcceptType = true;
          }
          if(acceptIs(accept.trim(), "xml")) {
            dstu3Parser = dstu3.newXmlParser();
            //supportedAcceptType = true;
          }
        }
      }

      if(pretty != null) {
        dstu3Parser = dstu3Parser.setPrettyPrint(Boolean.parseBoolean(pretty));
      }

      return dstu3Parser;
  }

}
