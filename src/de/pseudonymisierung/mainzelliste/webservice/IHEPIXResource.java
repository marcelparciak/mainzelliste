package de.pseudonymisierung.mainzelliste.webservice;

import java.io.IOException;
import java.lang.StringBuilder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.v25.message.ADT_AXX;
import ca.uhn.hl7v2.model.v25.datatype.TS;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.parser.CanonicalModelClassFactory;
import ca.uhn.hl7v2.parser.DefaultModelClassFactory;
import ca.uhn.hl7v2.util.Terser;

import org.apache.log4j.Logger;

import de.pseudonymisierung.mainzelliste.Config;
import de.pseudonymisierung.mainzelliste.Field;
import de.pseudonymisierung.mainzelliste.ID;
import de.pseudonymisierung.mainzelliste.IDGeneratorFactory;
import de.pseudonymisierung.mainzelliste.Patient;
import de.pseudonymisierung.mainzelliste.PlainTextField;
import de.pseudonymisierung.mainzelliste.IntegerField;
import de.pseudonymisierung.mainzelliste.Servers;
import de.pseudonymisierung.mainzelliste.dto.Persistor;

/**
 * Resources to communicate using the standard defined in IHE ITI-TF Vol1 and Vol2a
 */

@Path("/HL7")
public class IHEPIXResource {

  // set the two special fields that are set in mainzelliste.conf indicating which HL7 segments to construct an external ID
  private String specialConfigIDString = "externalIdString";
  private String specialConfigIDType = "externalIdType";

  /**
   * Logging
   */
  private Logger logger = Logger.getLogger(IHEPIXResource.class);

  @POST
  @Produces("x-application/hl7-v2+er7")
  public Response pixResponse(@Context HttpServletRequest req,
      String bodyString) {
      
      if(!Boolean.parseBoolean(Config.instance.getProperty("ihe.enabled"))) {
        // If ihe is not enabled, dont do anything
        return Response.status(404).build();
      }

      // A apiKey should be allowed to createSessions, createTokens, addPatients as well as editPatients
      Servers.instance.checkPermission(req, "iheAccess");
      Servers.instance.checkPermission(req, "patientFeed");

      logger.info("Received HL7 Message based on IHE ITI-8");


      HapiContext context = new DefaultHapiContext();
      // set the modelClassFactory according to HAPIs ExampleSuperStructures tutorial
      context.setModelClassFactory(new CanonicalModelClassFactory(ADT_AXX.class));
      // Use the PipeParser as the content type should be fixed by Servlet (or JAXRS? who know really)
      PipeParser p = context.getPipeParser();

      ADT_AXX generic_msg = null;
      try {
          generic_msg = (ADT_AXX) p.parse(bodyString);
      } catch(HL7Exception e) {
          logger.error("Supplied message is not a HL7 Message: " + bodyString);
          return Response.status(400).build();
      }

      // Switch to the default model class factory in order to provide an ACK (does not work for ADT_AXX messages otherwise)
      context.setModelClassFactory(new DefaultModelClassFactory());

      // Prepare the HL7 response ACK
      String response;
      try {
          generic_msg.generateACK();
          response = generic_msg.generateACK().encode();
      } catch(HL7Exception e) {
          e.printStackTrace();
          return Response.ok("Generating an ACK did not work.").build();
      } catch(IOException e) {
          //TODO: this is not a critical error I guess. Carry on maybe?
          return Response.ok("Some IO Error occured, but parsing worked.").build();
      }

      // Get the projected external ID and try to retrieve a Patient for it
      ID externalID = retrieveExternalId(generic_msg);
      Patient patient = Persistor.instance.getPatient(externalID);

      // Just respond with the generated ACK, if the Patient ID already exists
      // TODO: make an update here instead of just stopping
      if(patient != null) {
          logger.info("Requested External ID found.");
          return Response.ok(response).build();
      }

      logger.info("ID " + externalID.toString() + " is not stored. Creating a new Patient.");
      // create a map from the relevant HL7 segments
      Map<String, String> patData = createPatientDataMap(generic_msg);

      // Include the already created external ID to the patient
      patData.put(externalID.getType(), externalID.getIdString());
          
      // try to create a new patient with the constructed externalID
      if(!createNewPatient(patData, externalID)) {
          return Response.notModified("Could not create Patient for some reason.").build();
      }

      // well done!
      return Response.ok(response).build();
  }

  /**
   * Retrieves the patientid contained in the PID segment as a Mainzelliste ID.
   */
  private ID retrieveExternalId(ADT_AXX gen_msg) {
      // Terse the generic message
      Terser t = new Terser(gen_msg);

      String idType; 
      String idString;
      try {
        idType = t.get(Config.instance.getProperty("ihe.map." + this.specialConfigIDType));
        idString = t.get(Config.instance.getProperty("ihe.map." + this.specialConfigIDString));
      } catch(HL7Exception e) {
        logger.error("The externalIdString and externalIdType segments defined in mainzelliste.conf could not be read from the HL7 message.");
        return null;
      }
      if(idType == null) {
        idType = Config.instance.getProperty("ihe.pix.defaultIdGenerator");
      }

      return IDGeneratorFactory.instance.buildId(idType, idString);
  }

  /**
   * Creates a MultivaluedMap based on a PID segment of a HL7 ADT message
   * that conforms to the mainzelliste form input it requires to insert
   * a patient.
   */
  private Map<String, String> createPatientDataMap(ADT_AXX gen_msg) {
      Terser t = new Terser(gen_msg);
      Map<String, String> form = new HashMap<String, String>();

      for(String configKey: Config.instance.getProperties().stringPropertyNames()) {
        if(configKey.startsWith("ihe.map.")) {
          // Exclude the two special fields
          if(!configKey.equals("ihe.map." + this.specialConfigIDString)
              && !configKey.equals("ihe.map." + this.specialConfigIDType)) {
            
            // the configKey should now look like ihe.map.someField
            String fieldName = configKey.substring(8);
            // Terse the configured HL7 segment out of the message
            String hl7Value = null;
            try {
              hl7Value = t.get( Config.instance.getProperty(configKey) );
              if(hl7Value == null) {
                throw new HL7Exception("Not terseable / not populated");
              }
            } catch(HL7Exception e) {
              // not terseable or not populated, skip this value
              continue;
            }
            // retrieve the class of a Field
            Class<? extends Field<?>> fieldType = Config.instance.getFieldType(fieldName);

            // Case handling
            // Case 1: field is a date (starting with date.)
            if(fieldName.startsWith("date.")) {
              // Idea: get the dateformat and convert it using HAPI and java.text.SimpleDateFormat
              // TODO: does not work, cause Date as well as Calendar shift the year for some weird reason.
              String[] key_parts = fieldName.split("\\.");
              String formatString = 
                Config.instance.getProperty("validator.date." + key_parts[1] + ".format");

              List<String> partsOfDate = null;
              try {
                partsOfDate = this.hl7ToFormattedDateList(hl7Value, formatString, gen_msg);
              } catch(HL7Exception e) {
                logger.error("Could not parse the value " 
                    + hl7Value + " to " + formatString 
                    + " from the " + Config.instance.getProperty(configKey) + " segment.");
                continue;
              }

              String[] dateFields = 
                Config.instance.getProperty( "validator.date." + key_parts[1] + ".fields").split(",");
              if(partsOfDate.size() != dateFields.length) {
                logger.error("Cannot parse fields from the dateFormat String");
                continue;
              }

              for(int i = 0; i < dateFields.length; i++) {
                form.put(dateFields[i].trim(), partsOfDate.get(i));
              }
            } 
            // Case 2: field is a PlainText or Integer types
            else if(fieldType == PlainTextField.class || fieldType == IntegerField.class) {
              if(hl7Value != null) {
                form.put(fieldName, hl7Value);
              }
            }
            // Case 3: Unknown field, likely a configuration error.
            else {
              logger.error("Unkown type of configured field " + fieldName);
              continue;
            }
          }
        }
      }

      return form;
  }

  /**
   * Splits elements of an HL7 datestring (DTM) to a list of Strings based on a SimpleDateFormat.
   */
  private List<String> hl7ToFormattedDateList(String hapiString, String dateFormat, ADT_AXX gen_msg) throws HL7Exception {
    List<String> ls = new ArrayList<String>();

    // create a Date object from the hapiString
    TS hapiDate = new TS(gen_msg);
    Date date = null;
    try {
      hapiDate.parse(hapiString);
      date = hapiDate.getTime().getValueAsDate();
    } catch(DataTypeException e) {
      e.printStackTrace();
      logger.error("What happened?");
      return null;
    }
    // create the string date from the dateFormat
    SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
    String dateString = formatter.format(date);

    // the loop splits the format string on each new char (i.e. a new element of a date) 
    // and puts the formatted hapi date into the list. Basically, this loop iterates through two lists at the same time
    StringBuilder sb = new StringBuilder();
    char currentChar = 0;
    for(int i = 0; i < dateFormat.length(); i++) {
      if(dateFormat.charAt(i) != currentChar) {
        if(currentChar != 0) {
          ls.add(sb.toString());
          sb = new StringBuilder();
        }
      }
      currentChar = dateFormat.charAt(i);
      sb.append(dateString.charAt(i));
    }
    // add the last built string to the list aswell
    ls.add(sb.toString());
    logger.info(ls.toString());

    return ls;
  }

  /**
   * Handles creating a new patient including necessery IDs.
   *
   * This method is copied from PatientBackend.createNewPatient(); and 
   * consists of all instructions that are triggered when a NON_MATCH
   * is found, which should be equivalent to the creation and storage
   * of a new patient.
   */
  private boolean createNewPatient(Map<String, String> patientData, ID extId) {
      // read input fields from form
      Patient p = new Patient();
      // TODO: add necessary IDAT fields to patient
      Map<String, Field<?>> fields = new HashMap<String, Field<?>>();
      for(String s: Config.instance.getFieldKeys()) {
          if(patientData.containsKey(s)) {
              fields.put(s, Field.build(s, patientData.get(s)));
          }
      }
      p.setFields(fields);

      // normalize and transform
      Patient pNormalized = Config.instance.getRecordTransformer().transform(p);
      pNormalized.setInputFields(fields);

      boolean eagerGeneration = Boolean.parseBoolean(
                  Config.instance.getProperty("idgenerators.eagerGeneration") );
      Set<String> defaultId = new HashSet<String>();
      defaultId.add(IDGeneratorFactory.instance.getDefaultIDType());
      Set<ID> ids = eagerGeneration
              ? IDGeneratorFactory.instance.generateIds()
              : IDGeneratorFactory.instance.generateIds(defaultId);

      ids.add(extId);
      pNormalized.setIds(ids);
      logger.info("Created patient: " + p.toString());

      Persistor.instance.updatePatient(pNormalized);

      return true;
  }

}
