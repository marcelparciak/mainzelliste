package de.pseudonymisierung.mainzelliste;

import de.pseudonymisierung.mainzelliste.ID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * A Visit defines a three-way relation between two IDs and a patient.
 *
 * The idea behind a Visit is implementing the capability of generating IDs for an ID instead of a patient.
 * A common use case would be to generate Pseudonyms for visit identifiers for a patient, i.e.:
 * PatientVisit 1 - givenID 12345 (ExternalID given by the HIS), generatedID ABC123 (PID given by Mainzelliste)
 * PatientVisit 2 - givenID 54321 (ExternalID given by the HIS), generatedID DEF321 (PID given by Mainzelliste)
 * PatientVisit 3 - givenID 98765 (ExternalID given by the HIS), generatedID GHI567 (PID given by Mainzelliste)
 */
@Entity
public class Visit {

  /** database id. */
  @Id
  @GeneratedValue
  protected int visitJpaId;

  /**
   * The given ID for a visit, typically an ExternalID.
   */
  @OneToOne(cascade=CascadeType.ALL)
  ID givenID;

  /**
   * The generated ID by the mainzelliste, masking the givenID.
   */
  @OneToOne(cascade=CascadeType.ALL)
  ID generatedID;

  public Visit(ID givenID, ID generatedID) {
    this.givenID = givenID;
    this.generatedID = generatedID;
    //this.patient = patient;
  }

  public ID getGivenID() {
    return this.givenID;
  }

  public void setGivenID(ID id) {
    this.givenID = id;
  }

  public ID getGeneratedID() {
    return this.generatedID;
  }

  public void setGeneratedID(ID id) {
    this.generatedID = id;
  }

  public JSONObject toJSON() {
    try {
      JSONObject jsn = new JSONObject();
      JSONArray arr = new JSONArray();

      JSONObject given = this.givenID.toJSON().put("idType", "given");
      JSONObject generated = this.generatedID.toJSON().put("idType", "generated");
      arr.put(given).put(generated);

      jsn.put("ids", arr);
      return jsn;
    } catch(JSONException e) {
      throw new Error(e);
    }
  }
}
